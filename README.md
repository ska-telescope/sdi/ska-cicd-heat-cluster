# heat-cluster

Example template for handling dynamic inventory with Ansible to build a generic cluster.


## Summary

This repo contains an example of building a set of generic nodes on OpenStack driven by Ansible and Heat Templates.  It will build a cluster of master and worker nodes within their own Security Group, and then format with a basic Docker install.  Inspired by https://github.com/stackhpc/ansible-role-cluster-infra .

`/var/lib/docker` is placed in a separate volume, and by default a data volume is also created (see `playbooks/group_vars/generic` and `playbooks/roles/ska.systems.stackcluster/files/cluster-instance.yaml` for details).

The basic process to use is as follows:

source your `*-openrc.sh` file and test connectivity with something like `openstack server list`.

Populate your personal `PrivateRules.mak` with something like:
```
# File defining your cluster chemistry - see below
PRIVATE_VARS = private_vars.yml

# the relative file path to the dynamic inventory file that will be created
# and used by any subsequent roles applied
INVENTORY_FILE = ./inventory_test
```

Create your own local `private_vars.yml` (see `PRIVATE_VARS` above) file in the root of the project - eg:
```
---
# debug: true

# This name is used for the Heat stack and as a prefix for the
# cluster node hostnames.
cluster_name: piers_test # cluster resources name prefix
cluster_keypair: piers-engage

# mount point and volume size for generic data volume
data_filesystem: /data
data_vol_size: 10
# true - create and attach data volume - false, suppress creation
create_data_vol: true

genericnode_master:
  name: "master"
  flavor: "m1.medium"
  image: "Ubuntu-18.04-x86_64"
  num_nodes: 1

genericnode_worker:
  name: "worker"
  flavor: "m1.small"
  image: "Ubuntu-18.04-x86_64"
  num_nodes: 0

cluster_group_vars: # cluster wide defaults/overides
  cluster:
    ansible_user: ubuntu
    ansible_python_interpreter: python3
```

Review `playbooks/group_vars/generic` for more options and details.

To build use `make build`, to tear everything down again use `make clean`.

## Prerequisites

Ensure that your `ssh` can find the client certificate related to the `KEY_PAIR` used for the stack build eg: `${HOME}/.ssh/config` has been configured with:
```
...
IdentityFile /home/your-home/.ssh/yourkeypair-private-key.pem
...
```

Also, you need to make sure that the OpenStack account used has the `stack_owner`, as well as the `member` (sometimes `_member`) roles.
